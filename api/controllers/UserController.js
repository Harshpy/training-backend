/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const crypto = require('crypto');
var check;
var check2;

module.exports = {

    fetch: function (req, res) {
        User.find()
            .then(function (users) {
                res.send({
                    'message': users
                });
            })
    },
    adminupdate: function (req, res) {

        User.find({ where: { email: req.body.email } })
            .then(function (users) {
                if (req.body.email === check) {
                    var mykey = crypto.createDecipher('aes-128-cbc', 'password');
                    var mystr = mykey.update(users[0].password, 'hex', 'utf8')
                    mystr += mykey.final('utf8');
                    users[0].password = mystr;
                    check2 = 0

                    res.send(users[0]);

                }
                else if (check === null) {
                    var mykey = crypto.createDecipher('aes-128-cbc', 'password');
                    var mystr = mykey.update(users[0].password, 'hex', 'utf8')
                    mystr += mykey.final('utf8');
                    users[0].password = mystr;
                    res.send(users[0]);
                }
                else {
                    users[0].password = null;
                    check2 = null
                    res.send(users[0]);

                }


            })
    },
    create: function (req, res) {
        console.log(req.body)
        var mykey = crypto.createCipher('aes-128-cbc', 'password');
        var mystr = mykey.update(req.body.password, 'utf8', 'hex')
        mystr += mykey.final('hex');
        req.body.password = mystr;
        User.create(req.body)
            .then(function (user) {
                res.send({
                    'message': user
                });
            })
            .catch(function (err) {
                sails.log.debug(err);
            })
    },
    login: function (req, res) {
        let con;
        if (req.body.email && req.body.password) {
            var mykey = crypto.createCipher('aes-128-cbc', 'password');
            var mystr = mykey.update(req.body.password, 'utf8', 'hex')
            mystr += mykey.final('hex');
            req.body.password = mystr;
            User.find({ where: { email: req.body.email, password: req.body.password } })
                .then(function (users) {
                    if (!users || users.length == 0) {
                        con = true;
                        return res.send(con)
                    }
                    else if (users[0].role === "admin") {
                        User.find()
                            .then(function (admin) {
                                check = admin[0].email;
                                res.send(admin);

                            })

                    }
                    else {
                        User.find({ where: { email: req.body.email, password: req.body.password } })
                            .then(function (singleuser) {
                                check = null
                                res.send(singleuser);
                            })
                    }
                })
        }
    },
    update: (req, res) => {
        console.log(req.body);
        if (check != null && check2 === null) {
            let con
            console.log("amdin can't update user's password");
            con = true;
            res.send(con)

        }
        else {

            if (req.body.password) {
                var mykey = crypto.createCipher('aes-128-cbc', 'password');
                var mystr = mykey.update(req.body.password, 'utf8', 'hex')
                mystr += mykey.final('hex');
                req.body.password = mystr;
            }


            User.update({ where: { id: req.body.id } }, req.body)
                .then(function (users) {
                    res.send('details updated')
                })
                .catch((err) => {
                    console.log(err);
                })
        }

    },
    delete: (req, res) => {
        console.log(req.body);
        // User.destroy({ where: { id: req.body.id } })
        //     .then((user) => {
        //         return res.send('user deleted');
        //     })
        //     .catch((err) => {
        //         console.log(err);
        //     })
    }
};


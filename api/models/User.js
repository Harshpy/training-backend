/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id : {type : 'number', autoIncrement : true},
    firstname: { type: 'string', required: true},
    lastname: { type: 'string', required: false, allowNull : true },
    email: { type: 'string', required: true, unique : true},
    password:  { type: 'string', required: true },
    dob : {type : 'string', required : false, allowNull : true},
    role : {type : 'string', required : false, allowNull : true}

  },

};

